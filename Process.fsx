
#r "packages/FSharp.Data/lib/net40/FSharp.Data.dll"
#r "packages/Newtonsoft.Json/lib/net40/Newtonsoft.Json.dll"

open FSharp.Data

System.Environment.CurrentDirectory <- __SOURCE_DIRECTORY__

let createName (first:string) (last:string) = 
    let combined = first.Trim('"') + " " + last.Trim('"')
    combined.Trim()

type Class = Men | Women | Mixed
    

type Control = 
    {
    Number:int;
    Time: Option<int>;
    } member c.Points = c.Number - (c.Number % 10)

type Course =
    {
     Controls:Control Set
    }

type PersonRecord = 
    {
    ChipNo:int; 
    Name: string;
    Time: int;
    Class: Option<Class>;
    OriginalPoints: int;
    Penalty: int;
    OriginalTotal: int;
    Controls: Control list;
    } 
    member r.Points = r.Controls |> List.map (fun x -> x.Points) |> List.sum
    member r.Total = System.Math.Max(r.Points + r.Penalty, 0)

let readName parts = 
    createName (parts |> Array.item 6)  (parts |> Array.item 5)

let stringSplit char (str:string) = str.Split char

let parseTime s = 
    let t = s |> stringSplit [|':'|]
    match t.Length with
    | 3 ->
        let h = System.Int32.Parse t.[0]
        let m = System.Int32.Parse t.[1]
        let s = System.Int32.Parse t.[2]
        s + (m * 60) + (h * 60 * 60)
    | _ -> 0

let readTime parts =
    parts |> Array.item 13 |> parseTime

let readClass parts =
    let c = parts |> Array.item 28
    match c with
    | "M" -> Some(Men)
    | "F" -> Some(Women)
    | "X" -> Some(Mixed)
    | _ -> None

let parseInt n = 
    let success,i =  System.Int32.TryParse n
    if success then i
    else 
        failwithf "This sucks :( '%s'" (n.ToString())

let maxTime = 60 * 60

let firstControls = [31;43;46;53;32;56]

let readControls parts = 
    let rec Loop i acc =
        let numStr = parts |> Array.item i
        if numStr = "" then acc
        else
            let num = numStr |> parseInt
            let points = parts |> Array.item (i + 1)
            let time = parts.[i+2] |> parseTime |> (fun t -> if t > maxTime then None else Some(t))
            if points = "" then Loop (i + 3) acc
            else
                let ctrl = {
                    Number = numStr |> parseInt;
                    Time = time
                    }
                Loop (i + 3) (ctrl::acc)
    Loop 65 List.empty

let readLine (line:string) = 
    let parts = line.Split(',')
    {
    ChipNo = parts.[3] |> parseInt;
    Name = parts |> readName;
    Time = parts |> readTime;
    Class = parts |> readClass;
    OriginalPoints = parts.[59] |> parseInt;
    Penalty = parts.[60] |> parseInt;
    OriginalTotal = parts.[58] |> parseInt;
    Controls = parts |> readControls;
    }

let splitsResults file additionalControls = 
    let applyCorrections x = 
        let ctrls = additionalControls x.ChipNo
        match ctrls with
        | Some c -> {x with Controls = x.Controls |> List.append c}
        | None -> x
    
    System.IO.File.ReadAllLines file
    |> Array.skip 1 
    |> Array.map readLine 
    |> List.ofArray
    |> List.map applyCorrections

let noAdditionalControls r = None

let week1Results = 
    let additionalControls siNum =
        match siNum with
        | 420566 -> Some([{Number=37;Time=None}])
        | 420892 -> Some([{Number=46;Time=None}])
        | 7010250 -> Some([{Number=94;Time=None}])
        | _ -> None

    let results = splitsResults "splits.csv" additionalControls
    let acw = {
        ChipNo = 0;
        Name = "Al Cory-Wright"
        Time = 3510;
        Class = Some Men;
        OriginalPoints = 0;
        OriginalTotal = 0;
        Penalty = 0;
        Controls = [{Number=43;Time=None};{Number=46;Time=None};{Number=36;Time=None};{Number=61;Time=None};{Number=41;Time=None};{Number=76;Time=None};{Number=48;Time=None};{Number=47;Time=None};{Number=94;Time=None};{Number=82;Time=None};{Number=55;Time=None};{Number=45;Time=None};{Number=53;Time=None}]
        }
    acw::results    

let week2Results = 
    splitsResults "Splits-event2.csv" noAdditionalControls

let week3Results = 
    splitsResults "splits3.csv" noAdditionalControls

// let json = Newtonsoft.Json.JsonConvert.SerializeObject week1Results

type Overall = { Team:string; Class:Option<Class>; Week1:Option<int>; Week2:Option<int>; Week3:Option<int>} member x.Overall = [x.Week1;x.Week2;x.Week3] |> List.choose id |> List.sum
    
let overallResults w1 w2 w3 =
    let blank = {Team="";Class=None;Week1=None; Week2=None; Week3=None}
    let results = new System.Collections.Generic.Dictionary<string,Overall>(System.StringComparer.OrdinalIgnoreCase)
    w1 |> List.iter(fun x -> results.[x.Name] <- {blank with Team=x.Name; Class=x.Class; Week1=Some(x.Total)})
    w2 |> List.iter(fun x -> 
                    let exists, record = results.TryGetValue x.Name
                    if exists then 
                        results.[x.Name] <- {record with Team=x.Name; Class=x.Class; Week2=Some(x.Total)}                    
                    else
                        results.[x.Name] <- {blank with Team=x.Name; Class=x.Class; Week2=Some(x.Total)})
    w3 |> List.iter(fun x -> 
                    let exists, record = results.TryGetValue x.Name
                    if exists then 
                        results.[x.Name] <- {record with Team=x.Name; Class=x.Class; Week3=Some(x.Total)}                    
                    else
                        results.[x.Name] <- {blank with Team=x.Name; Class=x.Class; Week3=Some(x.Total)})
    results.Values |> List.ofSeq


let overall = overallResults week1Results week2Results week3Results

let printOverallResults r file = 
    let classOverallResults (s:System.Text.StringBuilder) c = 
        s.AppendLine "<table>" |> ignore
        s.AppendLine "<thead>" |> ignore
        s.AppendLine "<tr><th>Name</th><th>Week 1</th><th>Week 2</th><th>Week 3</th><th>Total</th></tr>" |> ignore
        s.AppendLine "</thead>" |> ignore
        s.AppendLine "<tbody>" |> ignore
        r 
        |> List.filter (fun x -> match x.Class with | Some y -> c = y | None -> false)
        |> List.sortByDescending (fun x -> x.Overall)
        |> Seq.iter(fun x -> 
                        let score x = 
                            match x with 
                            | None -> "-"
                            | Some y -> string y
                        s.AppendLine (sprintf "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%i</td></tr>" x.Team (score x.Week1) (score x.Week2) (score x.Week3) x.Overall) |> ignore)
        s.AppendLine "</tbody>" |> ignore
        s.AppendLine "</table>" |> ignore
    let dest = System.Text.StringBuilder()
    [(Women, "Women");(Men, "Men");(Mixed, "Mixed")]
    |> List.iter(fun (c,s) ->
                dest.AppendLine (sprintf "<h3>%s</h3>" s) |> ignore
                classOverallResults dest c
        )
    System.IO.File.WriteAllText (file, dest.ToString())


//let classResults results c =
//    results 
//    |> List.filter (fun x -> x.Class.Value = c) 
//    |> List.sortByDescending (fun x -> x.Overall)
//    |> List.take 5
//
//
//let men = classResults overall Men
//let women = classResults overall Women
//let mixed = classResults overall Mixed


let printEventResults r file =
    let classEventResults (s:System.Text.StringBuilder) c =        
        s.AppendLine "<table>" |> ignore
        s.AppendLine "<thead>" |> ignore
        s.AppendLine "<tr><th>Name</th><th>Points</th><th>Penalty</th><th>Total</th></tr>" |> ignore
        s.AppendLine "</thead>" |> ignore
        s.AppendLine "<tbody>" |> ignore
        r 
        |> List.filter (fun x -> x.Name |> ignore
                                 match x.Class with | Some y -> c = y | None -> false)
        |> List.sortByDescending (fun x -> x.Total)
        |> List.iter(fun x ->
                s.AppendLine (sprintf "<tr><td>%s</td><td>%i</td><td>%i</td><td>%i</td></tr>" x.Name x.Points x.Penalty x.Total)  |> ignore
            )
        s.AppendLine "</tbody>" |> ignore
        s.AppendLine "</table>" |> ignore

    let dest = System.Text.StringBuilder()
    [(Women, "Women");(Men, "Men");(Mixed, "Mixed")]
    |> List.iter(fun (c,s) ->
                dest.AppendLine (sprintf "<h3>%s</h3>" s) |> ignore
                classEventResults dest c
        )

    System.IO.File.WriteAllText (file, dest.ToString())


printEventResults week1Results "week1.html"
printEventResults week2Results "week2.html"
printEventResults week3Results "week3.html"
printOverallResults overall "overall.html"
    


//
//let nonCleared = 
//    results
//    |> List.filter (fun x -> x.OriginalPoints <> x.Points)
//    

//nonCleared |> List.iter (fun x -> printfn "%s %i %i" x.Name x.OriginalPoints x.Points)
//
//let nonClearedCount = nonCleared |> List.length